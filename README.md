# Chrome Extension Hot Reloader

Watches for file changes in your extension's directory. When a change is detected, it reloads the extension and refreshes the active tab (to re-trigger the updated scripts).


## Features

- Works by checking timestamps of files
- Supports nested directories
- Automatically disables itself in production
- And it's just a 50 lines of code!

Website:  https://www.jbbatterygermany.com/